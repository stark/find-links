#!/usr/bin/env python

from distutils.core import setup

setup(name = 'findlinks',
      version = '1.0',
      description = 'find symbolic links',
      author = 'Sebastian Stark',
      author_email = 'stark@tuebingen.mpg.de',
      py_modules = ['findlinks'],
      scripts = ['scripts/findlinks'],
     )
