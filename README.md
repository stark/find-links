find-links
==========

Find symlinks within a directory tree

Lists all symbolic links that could be potentially problematic with regard to
security or backup:

  deep:       links that point to other links (possible loops)
  loop:       links whose real paths are links too (loops)
  crosstree:  links that point to paths outside the tree (relative or absolute)
  absolute:   links that point to absolute paths
  void:       links that point to non-existent paths

Alpha grade software!

Tested with Python 2.7.6 on Ubuntu 14.04. Does not work with Python versions
below 2.7.

Installation
============

``pip install --user .``

Usage
=====

``find-links -h``

Tests
=====

Just execute ``./run-tests.sh`` in the distribution directory
