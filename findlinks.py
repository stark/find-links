#!/usr/bin/env python

from __future__ import print_function
import os

_epilog = 'See also: path_resolution(7), readlink(2), lstat(2)'

def checkLink(l, subtree=None):
    '''return dictionary with flags for the symlink under path l'''
    la = {
        'absolute': False,
        'deep': False,
        'loop': False,
        'crosstree': False,
        'void': False,
        'target': None,
        'realpath': None,
        'name': None,
    }
    absSubtree = os.path.abspath(subtree)
    la['name'] = l
    la['target'] = os.readlink(l)
    if la['target'].startswith('/'):
        la['absolute'] = True
    la['realpath'] = os.path.realpath(la['name'])
    target_fullpath = os.path.join(os.path.dirname(la['realpath']), la['target'])
    target_realpath = os.path.realpath(target_fullpath)
    if os.path.islink(target_fullpath):
        la['deep'] = True
    isfile = os.path.isfile(la['realpath'])
    isdir = os.path.isdir(la['realpath'])
    islink = os.path.islink(la['realpath'])
    if islink:
        la['loop'] = True
    # how about links to special files?
    if not (isdir or isfile):
        la['void'] = True
    if not la['realpath'].startswith(absSubtree):
        la['crosstree'] = True
    return la

def printLink(l):
    '''pretty print a flag dictionary'''
    print(l['name'], end='\t(')
    flags = [ k for k, v in l.items() if v and k not in ['name', 'target',
                                                            'realpath'] ]
    print(', '.join(flags), end='')
    #if l['void']: print(' [', l['realpath'], ']', end='', sep='')
    print(')')
