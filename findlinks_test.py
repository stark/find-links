#!/usr/bin/env python

import os
import sys
import unittest
import findlinks
import commands
import tempfile
import shutil

class TestCheckLink(unittest.TestCase):

    def setUp(self):
        self._tempdir = tempfile.mkdtemp(prefix='findlinks-tests')
        self.fn1 = os.path.join(self._tempdir, '1')
        self.fn2 = os.path.join(self._tempdir, '2')
        self.fn3 = os.path.join(self._tempdir, '3')
        self.fnl1 = os.path.join(self._tempdir, 'l1')
        self.fnl2 = os.path.join(self._tempdir, 'l2')
        open(self.fn1, 'a').close()

    def tearDown(self):
        shutil.rmtree(self._tempdir)

    def test_checkLink1(self):
        os.symlink(self.fn1, self.fn2)
        self.assertTrue(os.path.isfile(self.fn1))
        self.assertTrue(os.path.islink(self.fn2))
        self.assertTrue(os.path.realpath(self.fn2) == self.fn1)
        expected = {'realpath': self.fn1, 'target': self.fn1, 'void': False, 'deep':
                    False, 'name': self.fn2, 'crosstree': False, 'absolute': True,
                    'loop': False}
        la = findlinks.checkLink(self.fn2, subtree=self._tempdir)
        self.assertEqual(expected, la)

    def test_checkLink2(self):
        os.symlink('void', self.fn2)
        self.assertFalse(os.path.isfile('void'))
        self.assertTrue(os.path.islink(self.fn2))
        expected = {'realpath': os.path.join(self._tempdir, 'void'), 'target': 'void', 'void': True,
                    'deep': False, 'name': self.fn2, 'crosstree': False,
                    'absolute': False, 'loop': False}
        la = findlinks.checkLink(self.fn2, subtree=self._tempdir)
        self.assertEqual(expected, la)

    def test_checkLink3(self):
        os.symlink(self.fn1, self.fn2)
        self.assertTrue(os.path.islink(self.fn2))
        os.symlink('2', self.fn3)
        self.assertTrue(os.path.islink(self.fn3))
        expected = {'realpath': self.fn1, 'target': '2', 'void': False,
                    'deep': True, 'name': self.fn3, 'crosstree': False,
                    'absolute': False, 'loop': False}
        la = findlinks.checkLink(self.fn3, subtree=self._tempdir)
        self.assertEqual(expected, la)

    def test_checkLoop(self):
        os.symlink(self.fnl1, self.fnl2)
        self.assertTrue(os.path.islink(self.fnl2))
        os.symlink(self.fnl2, self.fnl1)
        self.assertTrue(os.path.islink(self.fnl1))
        expected = {'realpath': self.fnl1, 'target': self.fnl2, 'void': True,
                    'deep': True, 'name': self.fnl1, 'crosstree': False,
                    'absolute': True, 'loop': True}
        la = findlinks.checkLink(self.fnl1, subtree=self._tempdir)
        self.assertEqual(expected, la)

if __name__ == "__main__":
    unittest.main()
